FROM engageft/tini-base-8-jre-alpine:13
VOLUME /tmp
ARG IMAGE_VERSION
LABEL version=${IMAGE_VERSION}
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENV MAX_HEAP_SIZE -Xmx384m
CMD exec java $MAX_HEAP_SIZE $JAVA_OPTS -jar ./app.jar
