package com.engageft.consumer.messages;

import com.engageft.consumer.model.Auth;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.support.serializer.JsonSerde;

@Configuration
@EnableKafkaStreams
public class AuthFilteringStream {

    private final Logger logger = LoggerFactory.getLogger(AuthFilteringStream.class);

    @Value(value = "${auth.topic.name}")
    private String authTopicName;

    @Value(value = "${citi.auth.topic.name}")
    private String citibankAuthTopicName;

    @Bean
    public KStream<?, ?> kStream(StreamsBuilder kStreamBuilder) {

        logger.info("Entered auth streaming.");

        final KStream<String, Auth> input = kStreamBuilder.stream(authTopicName, Consumed.with(Serdes.String(), new JsonSerde<>(Auth.class)));

        input
            .filter((i, s) -> s.getTenant().equals("citi"))
            .to(citibankAuthTopicName, Produced.with(Serdes.String(), new JsonSerde<>(Auth.class)));

        input
            .filterNot((i, s) -> s.getTenant().equals("citi"))
            .foreach((key, auth) -> logger.info("~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!~!" + key + ": " + auth));

        return input;
    }

}