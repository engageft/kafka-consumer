package com.engageft.consumer.model;

public class Auth {

    private String tenant;
    private String processorReference;
    private String acquiringBankCode;
    private String networkReference;
    private String avsResponse;
    private String interchange;
    private String description;
    private String transactionType;
    private String posEntry;
    private String mcc;
    private String address;
    private String merchantId;
    private String merchantName;
    private String merchantNumber;
    private String merchantCity;
    private String merchantState;
    private String merchantZip;

    public Auth() {
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getProcessorReference() {
        return processorReference;
    }

    public void setProcessorReference(String processorReference) {
        this.processorReference = processorReference;
    }

    public String getAcquiringBankCode() {
        return acquiringBankCode;
    }

    public void setAcquiringBankCode(String acquiringBankCode) {
        this.acquiringBankCode = acquiringBankCode;
    }

    public String getNetworkReference() {
        return networkReference;
    }

    public void setNetworkReference(String networkReference) {
        this.networkReference = networkReference;
    }

    public String getAvsResponse() {
        return avsResponse;
    }

    public void setAvsResponse(String avsResponse) {
        this.avsResponse = avsResponse;
    }

    public String getInterchange() {
        return interchange;
    }

    public void setInterchange(String interchange) {
        this.interchange = interchange;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPosEntry() {
        return posEntry;
    }

    public void setPosEntry(String posEntry) {
        this.posEntry = posEntry;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantNumber() {
        return merchantNumber;
    }

    public void setMerchantNumber(String merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    public String getMerchantCity() {
        return merchantCity;
    }

    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    public String getMerchantState() {
        return merchantState;
    }

    public void setMerchantState(String merchantState) {
        this.merchantState = merchantState;
    }

    public String getMerchantZip() {
        return merchantZip;
    }

    public void setMerchantZip(String merchantZip) {
        this.merchantZip = merchantZip;
    }

    @Override
    public String toString() {
        return "Auth{" +
            "processorReference='" + processorReference + '\'' +
            ", acquiringBankCode='" + acquiringBankCode + '\'' +
            ", networkReference='" + networkReference + '\'' +
            ", avsResponse='" + avsResponse + '\'' +
            ", interchange='" + interchange + '\'' +
            ", description='" + description + '\'' +
            ", transactionType='" + transactionType + '\'' +
            ", posEntry='" + posEntry + '\'' +
            ", mcc='" + mcc + '\'' +
            ", address='" + address + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", merchantName='" + merchantName + '\'' +
            ", merchantNumber='" + merchantNumber + '\'' +
            ", merchantCity='" + merchantCity + '\'' +
            ", merchantState='" + merchantState + '\'' +
            ", merchantZip='" + merchantZip + '\'' +
            '}';
    }
}